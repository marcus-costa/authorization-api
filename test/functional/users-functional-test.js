
const assert = require('chai').assert;
const supertest = require('supertest');
const moment = require('moment');
const passwordHash = require('password-hash');
const app = require('../../lib/application');
const fixtures = require('../utils/fixtures');

describe('Testes funcional - Users', () => {
  describe('Casos de erro', () => {
    it('Deve retornar 401 quando não for informado o token', (done) => {
      supertest(app)
        .get('/users/1234567890')
        .expect('Content-Type', /json/)
        .end((err, res) => {
          assert.isNull(err);
          assert.equal(res.statusCode, 401);

          assert.property(res.body, 'message');
          assert.propertyVal(res.body, 'message', 'Unauthorized');

          done();
        });
    });

    it('Deve retornar 401 quando for informado um token inválido', (done) => {
      supertest(app)
        .get('/users/1234567890')
        .set('Authorization', 'bearer 3434343493904390439439043094')
        .expect('Content-Type', /json/)
        .end((err, res) => {
          assert.isNull(err);
          assert.equal(res.statusCode, 401);

          assert.property(res.body, 'message');
          assert.propertyVal(res.body, 'message', 'Unauthorized');

          done();
        });
    });

    it('Deve retornar 404 quando não for passado o id do usuário', (done) => {
      supertest(app)
        .get('/users')
        .set('Authorization', 'bearer 3434343493904390439439043094')
        .expect('Content-Type', /json/)
        .end((err, res) => {
          assert.isNull(err);
          assert.equal(res.statusCode, 404);

          assert.property(res.body, 'message');
          assert.propertyVal(res.body, 'message', 'Recurso não encontrado!');

          done();
        });
    });

    it('Deve retornar 401 quando o user existir mas for passado um id diferente', (done) => {
      const body = {
        email: 'oc.marcus@gmail.com',
        password: '12345677890',
      };

      fixtures.userManager(body, (errFixture, user) => {
        assert.isNull(errFixture);

        supertest(app)
          .get(`/users/${user.id}`)
          .set('Authorization', 'bearer fdfdfddfffdfdf')
          .expect('Content-Type', /json/)
          .end((err, res) => {
            assert.isNull(err);
            assert.equal(res.statusCode, 401);

            assert.property(res.body, 'message');
            assert.propertyVal(res.body, 'message', 'Unauthorized');

            done();
          });
      });
    });

    it('Deve retornar 401 quando o usuário existir o token for igual mas o id do for diferente', (done) => {
      const body = {
        email: 'oc.marcus@gmail.com',
        password: '12345677890',
      };

      fixtures.userManager(body, (errFixture, user) => {
        assert.isNull(errFixture);

        supertest(app)
          .get('/users/3445555')
          .set('Authorization', `bearer ${user.token}`)
          .expect('Content-Type', /json/)
          .end((err, res) => {
            assert.isNull(err);
            assert.equal(res.statusCode, 401);

            assert.property(res.body, 'message');
            assert.propertyVal(res.body, 'message', 'Unauthorized');

            done();
          });
      });
    });

    it('Deve retornar 401 quando o usuário existir o token e o id forem corretos mas o ultimo login a mais de 30 minutos', (done) => {
      const body = {
        email: 'oc.marcus@gmail.com',
        password: '12345677890',
        lastLogin: moment().subtract(31, 'minutes').toDate(),
      };

      fixtures.userManager(body, (errFixture, user) => {
        assert.isNull(errFixture);

        supertest(app)
          .get(`/users/${user.id}`)
          .set('Authorization', `bearer ${user.token}`)
          .expect('Content-Type', /json/)
          .end((err, res) => {
            assert.isNull(err);
            assert.equal(res.statusCode, 401);

            assert.property(res.body, 'message');
            assert.propertyVal(res.body, 'message', 'Unauthorized');

            done();
          });
      });
    });
  });

  describe('Casos de erro', () => {
    it('Deve retornar 200 quando o usuário existir o token e o user id forem corretos', (done) => {
      const body = {
        email: 'oc.marcus@gmail.com',
        password: '12345677890',
        lastLogin: moment().toDate(),
      };

      fixtures.userManager(body, (errFixture, user) => {
        assert.isNull(errFixture);

        supertest(app)
          .get(`/users/${user.id}`)
          .set('Authorization', `bearer ${user.token}`)
          .expect('Content-Type', /json/)
          .end((err, res) => {
            assert.isNull(err);
            assert.equal(res.statusCode, 200);

            assert.property(res.body, 'id');
            assert.property(res.body, 'name');
            assert.property(res.body, 'email');
            assert.equal(res.body.email, body.email);
            assert.property(res.body, 'phones');
            assert.notProperty(res.body, 'password');

            assert.property(res.body, 'token');
            assert.isTrue(passwordHash.verify(body.password, res.body.token));
            assert.property(res.body, 'createdAt');
            assert.property(res.body, 'updatedAt');
            assert.property(res.body, 'lastLogin');

            done();
          });
      });
    });
  });
});
