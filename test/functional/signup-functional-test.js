
const assert = require('chai').assert;
const supertest = require('supertest');
const sinon = require('sinon');
const passwordHash = require('password-hash');
const app = require('../../lib/application');
const models = require('../../lib/models');
const services = require('../../lib/services');

describe('Testes funcional - Signup', () => {
  let sandbox;

  before(() => {
    sandbox = sinon.sandbox.create();
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('Casos de erro', () => {
    it('Deve retornar formato de requisição inválido quando não for passado o nome, email e senha', (done) => {
      const body = {};
      supertest(app)
        .post('/signup')
        .send(body)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          assert.isNull(err);
          assert.equal(res.statusCode, 400);

          assert.property(res.body, 'message');
          assert.propertyVal(res.body, 'message', 'Formato de requisição inválido!');

          done();
        });
    });

    it('Deve retornar formato de requisição inválido quando não for passado somente a senha', (done) => {
      const body = {
        password: 'testtest',
      };
      supertest(app)
        .post('/signup')
        .send(body)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          assert.isNull(err);
          assert.equal(res.statusCode, 400);

          assert.property(res.body, 'message');
          assert.propertyVal(res.body, 'message', 'Formato de requisição inválido!');

          done();
        });
    });

    it('Deve retornar formato de requisição inválido quando for passado semente o email', (done) => {
      const body = {
        email: 'testtest',
      };
      supertest(app)
        .post('/signup')
        .send(body)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          assert.isNull(err);
          assert.equal(res.statusCode, 400);

          assert.property(res.body, 'message');
          assert.propertyVal(res.body, 'message', 'Formato de requisição inválido!');

          done();
        });
    });

    it('Deve retornar formato de requisição inválido quando for passado uma string', (done) => {
      const body = 'testtest';

      supertest(app)
        .post('/signup')
        .send(body)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          assert.isNull(err);
          assert.equal(res.statusCode, 400);

          assert.property(res.body, 'message');
          assert.propertyVal(res.body, 'message', 'Formato de requisição inválido!');

          done();
        });
    });

    it('Deve retornar formato de requisição inválido quando não for passado o nome', (done) => {
      const body = {
        email: 'oc.marcus@gmail.com',
        password: '12345677890',
      };
      supertest(app)
        .post('/signup')
        .send(body)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          assert.isNull(err);
          assert.equal(res.statusCode, 400);

          assert.property(res.body, 'message');
          assert.propertyVal(res.body, 'message', 'Formato de requisição inválido!');

          done();
        });
    });

    it('Deve retornar formato de requisição inválido quando o telefone não for um array', (done) => {
      const body = {
        name: 'marcus costa',
        email: 'oc.marcus@gmail.com',
        password: '12345677890',
        phones: '31988888888',
      };
      supertest(app)
        .post('/signup')
        .send(body)
        .end((err, res) => {
          assert.isNull(err);
          assert.equal(res.statusCode, 400);

          assert.property(res.body, 'message');
          assert.propertyVal(res.body, 'message', 'Formato de requisição inválido!');

          done();
        });
    });

    it('Deve retornar erro interno quando a busca do usuário falhar', (done) => {
      const body = {
        name: 'marcus costa',
        email: 'oc.marcus@gmail.com',
        password: '12345677890',
      };

      sandbox.stub(models, 'findUser').callsFake((email, callback) => {
        callback('Erro ao buscar', null);
      });

      supertest(app)
        .post('/signin')
        .send(body)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          assert.isNull(err);
          assert.equal(res.statusCode, 500);

          assert.property(res.body, 'message');
          assert.propertyVal(res.body, 'message', 'Internal server error');

          done();
        });
    });

    it('Deve retornar que o email já existe quando informado um email que já existe na base', (done) => {
      const body = {
        name: 'marcus costa',
        email: 'oc.marcus@gmail.com',
        password: '12345677890',
      };

      sandbox.stub(services, 'hasUser').callsFake((email, callback) => {
        callback(null, 1);
      });

      supertest(app)
        .post('/signup')
        .send(body)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          assert.isNull(err);

          assert.equal(res.statusCode, 409);

          assert.property(res.body, 'message');
          assert.propertyVal(res.body, 'message', 'User already registered');

          done();
        });
    });

    it('Deve retornar internal server error quando ocorrer erro no create user', (done) => {
      const body = {
        name: 'marcus costa',
        email: 'oc.marcus@gmail.com',
        password: '12345677890',
      };

      sandbox.stub(services, 'hasUser').callsFake((email, callback) => {
        callback(null, 0);
      });

      sandbox.stub(services, 'createUser').callsFake((user, callback) => {
        callback('error');
      });

      supertest(app)
        .post('/signup')
        .send(body)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          assert.isNull(err);

          assert.equal(res.statusCode, 500);

          assert.property(res.body, 'message');
          assert.propertyVal(res.body, 'message', 'Internal server error');

          done();
        });
    });

    it('Deve retornar internal server error quando ocorrer erro ao verificar se o usuário existe', (done) => {
      const body = {
        name: 'marcus costa',
        email: 'oc.marcus@gmail.com',
        password: '12345677890',
      };

      sandbox.stub(services, 'hasUser').callsFake((email, callback) => {
        callback('error');
      });

      supertest(app)
        .post('/signup')
        .send(body)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          assert.isNull(err);

          assert.equal(res.statusCode, 500);

          assert.property(res.body, 'message');
          assert.propertyVal(res.body, 'message', 'Internal server error');

          done();
        });
    });
  });

  describe('Casos de sucesso', () => {
    it('Deve criar o usuário quando ainda Não existir o mesmo usuário cadastrado', (done) => {
      const body = {
        name: 'marcus costa',
        email: 'marcus@gmail.com',
        password: '12345677890',
      };

      supertest(app)
        .post('/signup')
        .send(body)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          assert.isNull(err);

          assert.equal(res.statusCode, 201);

          assert.property(res.body, 'id');
          assert.property(res.body, 'name');
          assert.property(res.body, 'email');
          assert.equal(res.body.email, body.email);
          assert.notProperty(res.body, 'password');

          assert.property(res.body, 'token');
          assert.isTrue(passwordHash.verify(body.password, res.body.token));
          assert.property(res.body, 'createdAt');
          assert.property(res.body, 'updatedAt');
          assert.property(res.body, 'lastLogin');

          done();
        });
    });
  });
});
