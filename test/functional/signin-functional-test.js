
const assert = require('chai').assert;
const supertest = require('supertest');
const sinon = require('sinon');
const passwordHash = require('password-hash');

const app = require('../../lib/application');
const fixtures = require('../utils/fixtures');
const models = require('../../lib/models');

describe('Testes funcional - Signin', () => {
  let sandbox;

  before(() => {
    sandbox = sinon.sandbox.create();
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('Casos de erro', () => {
    it('Deve retornar formato de requisição inválido quando não for passado o email e senha', (done) => {
      const body = {};
      supertest(app)
        .post('/signin')
        .send(body)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          assert.isNull(err);
          assert.equal(res.statusCode, 400);

          assert.property(res.body, 'message');
          assert.propertyVal(res.body, 'message', 'Formato de requisição inválido!');

          done();
        });
    });

    it('Deve retornar formato de requisição inválido quando não for passado o email', (done) => {
      const body = {
        password: 'testtest',
      };
      supertest(app)
        .post('/signin')
        .send(body)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          assert.isNull(err);
          assert.equal(res.statusCode, 400);

          assert.property(res.body, 'message');
          assert.propertyVal(res.body, 'message', 'Formato de requisição inválido!');

          done();
        });
    });

    it('Deve retornar formato de requisição inválido quando não for passado a senha', (done) => {
      const body = {
        email: 'testtest',
      };
      supertest(app)
        .post('/signin')
        .send(body)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          assert.isNull(err);
          assert.equal(res.statusCode, 400);

          assert.property(res.body, 'message');
          assert.propertyVal(res.body, 'message', 'Formato de requisição inválido!');

          done();
        });
    });

    it('Deve retornar formato de requisição inválido quando for passado uma string', (done) => {
      const body = 'testtest';

      supertest(app)
        .post('/signin')
        .send(body)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          assert.isNull(err);
          assert.equal(res.statusCode, 400);

          assert.property(res.body, 'message');
          assert.propertyVal(res.body, 'message', 'Formato de requisição inválido!');

          done();
        });
    });

    it('Deve retornar usuário ou senha inválidos quando não existir o usuário cadastrado', (done) => {
      const body = {
        email: 'oc.marcus@gmail.com',
        password: '12345677890',
      };
      supertest(app)
        .post('/signin')
        .send(body)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          assert.isNull(err);
          assert.equal(res.statusCode, 404);

          assert.property(res.body, 'message');
          assert.propertyVal(res.body, 'message', 'User or password is invalid');

          done();
        });
    });

    it('Deve retornar erro interno quando a busca do usuário falhar', (done) => {
      const body = {
        email: 'oc.marcus@gmail.com',
        password: '12345677890',
      };

      sandbox.stub(models, 'findUser').callsFake((email, callback) => {
        callback('Erro ao buscar', null);
      });

      supertest(app)
        .post('/signin')
        .send(body)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          assert.isNull(err);

          assert.equal(res.statusCode, 500);

          assert.property(res.body, 'message');
          assert.propertyVal(res.body, 'message', 'Internal server error');

          done();
        });
    });
  });

  describe('Casos de sucesso', () => {
    it('Deve retornar dados do usuário quando o usuário é encontrado e a senha é valida', (done) => {
      const body = {
        email: 'oc.marcus@gmail.com',
        password: '12345677890',
      };

      fixtures.userManager(body, (errFixture) => {
        assert.isNull(errFixture);

        supertest(app)
          .post('/signin')
          .send(body)
          .expect('Content-Type', /json/)
          .end((err, res) => {
            assert.isNull(err);

            assert.equal(res.statusCode, '200');

            assert.property(res.body, 'id');
            assert.property(res.body, 'name');
            assert.property(res.body, 'email');
            assert.equal(res.body.email, body.email);
            assert.property(res.body, 'phones');
            assert.notProperty(res.body, 'password');

            assert.property(res.body, 'token');
            assert.isTrue(passwordHash.verify(body.password, res.body.token));
            assert.property(res.body, 'createdAt');
            assert.property(res.body, 'updatedAt');
            assert.property(res.body, 'lastLogin');

            done();
          });
      });
    });
  });
});
