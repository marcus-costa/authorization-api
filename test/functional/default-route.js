
const assert = require('chai').assert;
const supertest = require('supertest');
const app = require('../../lib/application');

describe('Default route', () => {
  it('Deve retornar recurso não encontrado quando a rota não existir (post)', (done) => {
    const body = {};
    supertest(app)
      .post('/')
      .send(body)
      .expect('Content-Type', /json/)
      .expect(404)
      .end((err, res) => {
        assert.property(res.body, 'message');
        assert.propertyVal(res.body, 'message', 'Recurso não encontrado!');

        done();
      });
  });

  it('Deve retornar recurso não encontrado quando a rota não existir (get)', (done) => {
    supertest(app)
      .get('/')
      .expect('Content-Type', /json/)
      .expect(404)
      .end((err, res) => {
        assert.property(res.body, 'message');
        assert.propertyVal(res.body, 'message', 'Recurso não encontrado!');

        done();
      });
  });

  it('Deve retornar recurso não encontrado quando a rota não existir (put)', (done) => {
    const body = {};
    supertest(app)
      .put('/')
      .send(body)
      .expect('Content-Type', /json/)
      .expect(404)
      .end((err, res) => {
        assert.property(res.body, 'message');
        assert.propertyVal(res.body, 'message', 'Recurso não encontrado!');

        done();
      });
  });

  it('Deve retornar recurso não encontrado quando a rota não existir (patch)', (done) => {
    const body = {};
    supertest(app)
      .patch('/')
      .send(body)
      .expect('Content-Type', /json/)
      .expect(404)
      .end((err, res) => {
        assert.property(res.body, 'message');
        assert.propertyVal(res.body, 'message', 'Recurso não encontrado!');

        done();
      });
  });

  it('Deve retornar recurso não encontrado quando a rota não existir (delete)', (done) => {
    supertest(app)
      .delete('/tests')
      .expect('Content-Type', /json/)
      .expect(404)
      .end((err, res) => {
        assert.property(res.body, 'message');
        assert.propertyVal(res.body, 'message', 'Recurso não encontrado!');

        done();
      });
  });
});
