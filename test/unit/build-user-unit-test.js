
const assert = require('chai').assert;
const fixtures = require('../utils/fixtures');
const passwordHash = require('password-hash');
const buildUser = require('../../lib/services/build-user');

describe('Testes unitários - Build user', () => {
  it('Deve criar um usuário válido com todas as propriedades do contrato', () => {
    const user = buildUser(fixtures.userFixture({}));
    assert.property(user, 'id');
    assert.property(user, 'name');
    assert.property(user, 'email');
    assert.property(user, 'phones');
    assert.notProperty(user, 'password');
    assert.property(user, 'token');
    assert.isTrue(passwordHash.isHashed(user.token));
    assert.property(user, 'createdAt');
    assert.property(user, 'updatedAt');
    assert.property(user, 'lastLogin');
  });
});
