const models = require('../../../lib/models');
const userFixture = require('./user-fixture');

module.exports = (data, callback) => {
  const fixture = userFixture(data);

  models.insertUser(fixture, (err, user) => {
    if (err) {
      return callback(err);
    }
    callback(null, user);
  });
};
