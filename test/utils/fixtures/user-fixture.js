const Chance = require('chance');
const passwordHash = require('password-hash');

const chance = new Chance();

module.exports = (data = {}) => {
  const user = {};

  user.id = data.id || chance.hash({ length: 25 });
  user.name = data.name || chance.name({ middle: true });
  user.email = data.email || chance.email({ domain: 'example.com' });
  user.password = data.password || chance.hash({ length: 25 });
  user.phones = data.phones || [
    {
      phone: chance.phone(),
      ddd: chance.integer({ min: 20, max: 30 }),
    }];
  user.createdAt = data.createdAt || chance.date({ string: true });
  user.updatedAt = data.updatedAt || chance.date({ string: true });
  user.lastLogin = data.lastLogin || chance.date({ string: true });
  user.token = passwordHash.generate(data.password || chance.hash({ length: 25 }));

  return user;
};
