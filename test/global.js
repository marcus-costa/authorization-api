const conf = require('../lib/commons/conf');
const db = require('../lib/commons/db');

before((done) => {
  db.connect(conf.get('MONGO_URL_TEST'), done);
});

after((done) => {
  db.getCollection('users').remove({}, () => {
    db.close(done);
  });
});
