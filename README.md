#Authorization-api

[![CircleCI](https://circleci.com/bb/marcus-costa/authorization-api.svg?style=svg)](https://circleci.com/bb/marcus-costa/authorization-api)

## heroku: `https://frozen-shelf-88515.herokuapp.com`

## signup 
### Methods `POST`
### url params `userId`
### Data Params


```
#!json

{
	"email": "exemple@gmail.com",
	"password": "password123",
	"name": "John",
        "phones":[{
              "ddd": 11,
              "number": "34567-3234"
        }]
}
// phones is optional

```

### Success Response
`201`
#### Content:


```
#!json

{
    "id": "553a2d3d-f552-43e9-8373-fa666572579b",
    "name": "John",
    "email": "exemple@gmail.cocm",
    "token": "sha1$354d4a52$1$5a1d32eb224e3ba2af5f1301441903293bd6bf3e",
    "createdAt": "2017-06-24T18:09:19.456Z",
    "updatedAt": "2017-06-24T18:09:19.456Z",
    "lastLogin": "2017-06-24T18:09:19.457Z"
}

```

### Error Response

`409 (Conflict)`
#### Content:

```
#!json
{
    "message": "User already registered"
}

```

#### Content:
`500 (Internal server error)`
```
#!json
{
    "message": "Internal server error"
}

```


## signin 
### Methods `POST`
### Data Params


```
#!json

{
	"email": "exemple@gmail.com",
	"password": "password123"
}

```

### Success Response
`200`
#### Content:


```
#!json

{
    "id": "553a2d3d-f552-43e9-8373-fa666572579b",
    "name": "John",
    "email": "exemple@gmail.cocm",
    "token": "sha1$354d4a52$1$5a1d32eb224e3ba2af5f1301441903293bd6bf3e",
    "createdAt": "2017-06-24T18:09:19.456Z",
    "updatedAt": "2017-06-24T18:09:19.456Z",
    "lastLogin": "2017-06-24T18:09:19.457Z"
}

```

### Error Response

`404 (Not found)`
#### Content:

```
#!json
{
    "message": "User or password is invalid"
}

```

#### Content:
`500 (Internal server error)`
```
#!json
{
    "message": "Internal server error"
}

```

## users 
### Methods `GET`
### Url Params
- userId
### Headers
- Authorization bearer {token}

### Success Response
`200`
#### Content:


```
#!json

{
    "id": "553a2d3d-f552-43e9-8373-fa666572579b",
    "name": "John",
    "email": "exemple@gmail.cocm",
    "token": "sha1$354d4a52$1$5a1d32eb224e3ba2af5f1301441903293bd6bf3e",
    "createdAt": "2017-06-24T18:09:19.456Z",
    "updatedAt": "2017-06-24T18:09:19.456Z",
    "lastLogin": "2017-06-24T18:09:19.457Z"
}

```

### Error Response

`401 (Unauthorized)`
#### Content:

```
#!json
{
    "message": "Unauthorized"
}

```

#### Content:
`500 (Internal server error)`
```
#!json
{
    "message": "Internal server error"
}

```