process.title = 'authorization-api';
const app = require('./lib/application');
const logger = require('./lib/commons/logger');
const conf = require('./lib/commons/conf');
const db = require('./lib/commons/db');

const appPort = conf.get('APP_PORT');

db.connect(conf.get('DB_URL'), (dbErr) => {
  if (dbErr) {
    logger.error('Unable to connect to Mongo.');
    process.exit(1);
  } else {
    app.listen(process.env.PORT || appPort, (err) => {
      if (err) {
        logger.error('Error on listen port ', appPort);
      } else {
        logger.info('Server listening on port ', appPort);
      }
    });
  }
});
