const services = require('../../services');

module.exports = (token, next) => {
  services.findUserByToken(token, (err, user) => {
    if (err) {
      return next(err);
    } else if (!user) {
      return next(null, false);
    }
    next(null, user, { token });
  });
};
