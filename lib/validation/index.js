
const tv4 = require('tv4');
const signinSchema = require('./schemas/signin-schema');
const signupSchema = require('./schemas/signup-schema');

tv4.addSchema(signinSchema);
tv4.addSchema(signupSchema);

const validate = (schema, req, res, next) => {
  const result = tv4.validateMultiple(req.body, schema);
  if (!result.valid) {
    res.status(400).json({ message: 'Formato de requisição inválido!' });
  } else {
    next();
  }
};

const signin = (req, res, next) => {
  validate('signin', req, res, next);
};

const signup = (req, res, next) => {
  validate('signup', req, res, next);
};

module.exports = {
  signin,
  signup,
};
