
const express = require('express');
const compress = require('compression');
const bodyParser = require('body-parser');
const routes = require('./routes');
const passport = require('passport');
const BearerStrategy = require('passport-http-bearer');
const authentication = require('./commons/authentication');

const app = express();
app.use(compress());
app.use(bodyParser.json());
passport.use(new BearerStrategy(authentication));
app.use(routes);

module.exports = app;
