
const express = require('express');
const controllers = require('./controllers');
const validate = require('./validation');
const passport = require('passport');

const router = new express.Router();

router.post('/signin', validate.signin, controllers.signin);
router.post('/signup', validate.signup, controllers.signup);

router.get('/users/:userId', (req, res, next) => {
  passport.authenticate('bearer', (err, user, info) => {
    if (err) {
      return next(err);
    }
    if (!user) {
      return res.status(401).json({ message: 'Unauthorized' });
    }
    req.user = user;
    req.authInfo = info;
    next();
  })(req, res, next);
}, controllers.users.get);

router.all('*', (req, res) => {
  res.status(404).json({ message: 'Recurso não encontrado!' });
});

module.exports = router;
