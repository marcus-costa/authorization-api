const db = require('./commons/db');
const logger = require('./commons/logger');

const findUser = (criteria, callback) => {
  db.getCollection('users').findOne(criteria, (err, user) => {
    if (err) {
      logger.error('Error by searching user');
    }

    callback(err, user);
  });
};

const updateUser = (email, params, callback) => {
  const where = {
    email,
  };

  const update = {
    $set: params,
  };

  const options = {
    new: true,
  };

  db.getCollection('users').findAndModify(where, {}, update, options, (err, user) => {
    if (err) {
      logger.error('Error by updating user %j', err);
    }

    callback(err, user.value);
  });
};

const countUser = (criteria, callback) => {
  db.getCollection('users').count(criteria, (err, count) => {
    if (err) {
      logger.error('Error by searching user');
    }

    callback(err, count);
  });
};

const insertUser = (user, callback) => {
  db.getCollection('users').insert(user, (err) => {
    if (err) {
      logger.error('Error by inserting user');
    }

    callback(err, user);
  });
};

module.exports = {
  findUser,
  insertUser,
  countUser,
  updateUser,
};
