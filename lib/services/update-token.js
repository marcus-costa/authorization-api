const passwordHash = require('password-hash');
const moment = require('moment');
const models = require('../models');

module.exports = (user, callback) => {
  models.updateUser(user.email,
    {
      token: passwordHash.generate(user.password),
      updatedAt: moment().toDate(),
      lastLogin: moment().toDate(),
    }, callback);
};
