const models = require('../models');
const moment = require('moment');

module.exports = (token, callback) => {
  const criteria = {
    token,
    lastLogin: {
      $gte: moment().subtract(30, 'minutes').toDate(),
    },
  };
  models.findUser(criteria, callback);
};
