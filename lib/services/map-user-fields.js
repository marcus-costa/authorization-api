
module.exports = user => (
  {
    id: user.id,
    name: user.name,
    email: user.email,
    phones: user.phones,
    token: user.token,
    createdAt: user.createdAt,
    updatedAt: user.updatedAt,
    lastLogin: user.lastLogin,
  }
);
