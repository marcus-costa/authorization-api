const models = require('../models');
const buildUser = require('./build-user');

module.exports = (user, callback) => {
  const newUser = buildUser(user);

  models.insertUser(newUser, callback);
};
