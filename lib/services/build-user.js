const moment = require('moment');
const uuidv4 = require('uuid/v4');
const passwordHash = require('password-hash');

module.exports = user => (
  {
    id: uuidv4(),
    name: user.name,
    email: user.email,
    token: passwordHash.generate(user.password),
    phones: user.phones,
    createdAt: moment().toDate(),
    updatedAt: moment().toDate(),
    lastLogin: moment().toDate(),
  });
