const models = require('../models');

module.exports = (email, callback) => {
  const criteria = {
    email,
  };
  models.countUser(criteria, callback);
};
