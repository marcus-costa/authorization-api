const models = require('../models');

module.exports = (email, callback) => {
  const criteria = {
    email,
  };
  models.findUser(criteria, callback);
};
