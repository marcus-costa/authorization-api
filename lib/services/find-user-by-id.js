const models = require('../models');

module.exports = (id, callback) => {
  const criteria = {
    id,
  };
  models.findUser(criteria, callback);
};
