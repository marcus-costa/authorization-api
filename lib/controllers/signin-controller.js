const passwordHash = require('password-hash');
const services = require('../services');

module.exports = (req, res) => {
  const body = req.body;

  services.findUserByEmail(body.email, (err, user) => {
    if (err) {
      res.status(500).json({ message: 'Internal server error' });
    } else if (user && passwordHash.verify(body.password, user.token)) {
      services.updateToken(body, (errUpdate, newUser) => {
        if (errUpdate) {
          res.status(500).json({ message: 'Internal server error' });
        } else {
          res.status(200).json(services.userMapFields(newUser));
        }
      });
    } else {
      res.status(404).json({ message: 'User or password is invalid' });
    }
  });
};
