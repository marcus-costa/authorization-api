
const services = require('../services');

module.exports = (req, res) => {
  const body = req.body;

  services.hasUser(body.email, (err, hasUser) => {
    if (err) {
      res.status(500).json({ message: 'Internal server error' });
    } else if (hasUser) {
      res.status(409).json({ message: 'User already registered' });
    } else {
      services.createUser(body, (errInsert, user) => {
        if (errInsert) {
          res.status(500).json({ message: 'Internal server error' });
        } else {
          res.status(201).json(services.userMapFields(user));
        }
      });
    }
  });
};
