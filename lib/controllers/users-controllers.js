
const services = require('../services');

const get = (req, res) => {
  const userId = req.params.userId;
  const user = req.user;
  const token = req.authInfo.token;

  if (userId === user.id && token === user.token) {
    res.json(services.userMapFields(req.user));
  } else {
    res.status(401).json({ message: 'Unauthorized' });
  }
};

module.exports = {
  get,
};
